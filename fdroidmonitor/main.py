# SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
# SPDX-License-Identifier: AGPL-3.0-or-later

import argparse
import datetime
import email.utils
import json
import os
import signal
import traceback
import humanize
from collections import defaultdict
from xml.etree import ElementTree as ET

import tornado.gen
import tornado.httpclient
import tornado.ioloop
import tornado.template
import tornado.web
from tornado.escape import json_encode

import fdroidmonitor
import fdroidmonitor.cfg
import fdroidmonitor.fetcher
import fdroidmonitor.services
import fdroidmonitor.tls_certs
import fdroidmonitor.ui_methods

AF_NAMES = {
    "Ads": "Advertising",
    "Tracking": "Tracking",
    "NonFreeNet": "Non-Free Network Services",
    "NonFreeAdd": "Non-Free Addons",
    "NonFreeDep": "Non-Free Dependencies",
    "UpstreamNonFree": "Upstream Non-Free",
    "NonFreeAssets": "Non-Free Assets",
    "KnownVuln": "Known Vulnerability",
    "DisabledAlgorithm": "Disabled Algorithm",
    "NoSourceSince": "No Source Since",
}

CACHE = {
    "liberapay": {
        "timestamp": 0,
        "value": None,
    },
    "opencollective": {
        "timestamp": 0,
        "value": None,
    },
    "eurofxref": {
        "timestamp": 0,
        "value": None,
    },
}
CACHE_DURATION = 60 * 60
DATE_FMT = "%Y-%m-%d %H:%M:%S %Z"


def template_path(template):
    return os.path.join(
        os.path.abspath(os.path.dirname(__file__)), "templates", template
    )


async def load_status(title):
    body, last_modified = await fdroidmonitor.fetcher.get(
        f"https://f-droid.org/repo/status/{title}.json"
    )
    d = json.loads(body)
    end_time = None
    if "endTimestamp" in d:
        end_time = datetime.datetime.fromtimestamp(
            d["endTimestamp"] // 1000, tz=datetime.timezone.utc
        )
    start_time = datetime.datetime.fromtimestamp(
        d["startTimestamp"] // 1000, tz=datetime.timezone.utc
    )
    last_modified = datetime.datetime(
        *email.utils.parsedate(last_modified)[:6], tzinfo=datetime.timezone.utc
    )
    data = {
        "title": title,
        "data": d,
        "lastModified": last_modified.strftime(DATE_FMT)
        + " ("
        + humanize.naturaltime(
            last_modified,
            when=datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc),
        )
        + ')',
        "startTime": start_time.strftime(DATE_FMT)
        + " ("
        + humanize.naturaltime(
            start_time,
            when=datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc),
        )
        + ")",
        "endTime": (
            end_time.strftime(DATE_FMT)
            + " ("
            + humanize.naturaltime(
                end_time,
                when=datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc),
            )
            + ")"
            if end_time
            else None
        ),
        "dataSources": [f"https://f-droid.org/repo/status/{title}.json"],
        "guestVagrantVmCpus": d.get("guestVagrantVmCpus"),
        "guestVagrantVmMemory": d.get("guestVagrantVmMemory"),
    }
    return data


class MyException(Exception):
    def __init__(self, msg="", desc="", status=500):
        super().__init__(msg)
        self.desc = desc
        self.status = status


class BaseRequestHandler(tornado.web.RequestHandler):
    def get_template_namespace(self):
        namespace = super().get_template_namespace()
        cfg = fdroidmonitor.cfg.load()
        namespace.update(
            {
                "monitorVersion": fdroidmonitor.VERSION,
                "cfg": cfg,
                "format_json": lambda v: json.dumps(v, indent=2),
            }
        )
        return namespace

    # def on_finish(self):
    #     cfg = fdroidmonitor.cfg.load()
    #     if cfg.get("debug", None):
    #       print(self.request.method, self.get_status(), self.request.path)

    def write_error(self, status_code, **kwargs):
        ex = kwargs["exc_info"][1]
        tb = kwargs["exc_info"][2]
        data = {"err_title": str(ex), "err_text": "", "traceback": ""}
        cfg = fdroidmonitor.cfg.load()
        if cfg.get("debug", ""):
            data["traceback"] = "".join(traceback.format_exception(None, ex, tb))
        if isinstance(ex, MyException):
            data["err_text"] = ex.desc
            self.set_status(ex.status)
        self.render(template_path("err.html"), **data)


class Err404Handler(BaseRequestHandler):
    def prepare(self):
        data = {
            "err_title": "HTTP 404: Not Found",
            "err_text": f"{self.request.path} not found",
            "traceback": "",
        }
        self.set_status(404)
        self.render(template_path("err.html"), **data)


class IndexHandler(BaseRequestHandler):
    def get(self):
        self.redirect("/builds")


class BuildsHandler(BaseRequestHandler):
    def get(self):
        data = {"title": "This is TITLE"}
        self.render(template_path("builds.html"), **data)


def dataCommandLineFix(data):
    # workaround for inconsisten commandLine value which sometimes is a
    # string somtimes an array
    if (
        'commandLine' in data.get('data', {})
        and type(data.get('data', {}).get('commandLine')) == list
    ):
        data['data']['commandLine'] = " ".join(data['data']['commandLine'])


class StatusHandler(BaseRequestHandler):
    async def get(self, title):
        data = await load_status(title)
        dataCommandLineFix(data)
        self.render(template_path("build.html"), **data)


class WebsiteHandler(BaseRequestHandler):
    async def get(self):
        data = await load_status('deploy-to-f-droid.org')
        dataCommandLineFix(data)
        self.render(template_path("website.html"), **data)


class ServicesHandler(BaseRequestHandler):
    async def get(self):
        data = {
            "defs": fdroidmonitor.services.SERVICE_DEFS,
            "sections": fdroidmonitor.services.SECTION_DEFS,
            "timeseries": fdroidmonitor.services.SERVICE_TIMESERIES,
        }
        self.render(template_path("services.html"), **data)


class TlsCertsHandler(BaseRequestHandler):
    async def get(self):
        data = {
            "title": "TLS Certificate Transparency Monitoring",
            "tlsCerts": fdroidmonitor.tls_certs.TLS_CERTS,
            "tlsExpected": fdroidmonitor.tls_certs.EXPECTED_TLS_CERTS,
            "dataSources": ["https://sslmate.com/help/reference/ct_search_api_v1"],
        }
        self.render(template_path("tls-certs.html"), **data)


class DisabledHandler(BaseRequestHandler):
    async def get(self):
        update = await load_status("update")
        data = {
            "title": "Disabled apps",
            "text": "These are disabled - they don't show in the client or the "
            "web repo (and probably never will).",
            "apps": update["data"].get("disabled", []),
            "lastModified": update["lastModified"],
            "dataSources": update["dataSources"],
        }
        self.render(template_path("applist.html"), **data)


class ArchivedHandler(BaseRequestHandler):
    async def get(self):
        update = await load_status("update")
        data = {
            "title": "Archived apps",
            "text": "These are archived - they only show in the client if the "
            "archive repository is enabled.",
            "apps": update["data"].get("archivePolicy0", []),
            "lastModified": update["lastModified"],
            "dataSources": update["dataSources"],
        }
        self.render(template_path("applist.html"), **data)


class NeedsUpdateHandler(BaseRequestHandler):
    async def get(self):
        update = await load_status("update")
        data = {
            "title": "Need updating",
            "text": "These apps need updates – we don't have the current "
            "version (the latest version recommended by the developers for "
            "general use and FOSS). Note that, for most apps, we do "
            "automatic checks for new versions. The checks are not flawless, "
            "however: developers can forget to tag a release; source code may "
            "have been withheld or moved to a new location; repository structure "
            "changes can confuse the checks; etc. Please let us know if you "
            "think that the current version is wrong.",
            "apps": update["data"].get("needsUpdate", []),
            "lastModified": update["lastModified"],
            "dataSources": update["dataSources"],
        }
        self.render(template_path("applist.html"), **data)


class NoUpdateCheckHandler(BaseRequestHandler):
    async def get(self):
        update = await load_status("update")
        data = {
            "title": "No update check",
            "text": "These have Update Check Mode set to None, so we can't "
            "automatically know when new versions are available. We need to "
            "implement new methods of checking, and check manually in the "
            "meantime. Apps which likely won't ever be updated, or for which the "
            "author would let us know personally, don't appear here.",
            "apps": update["data"].get("noUpdateCheck", []),
            "lastModified": update["lastModified"],
            "dataSources": update["dataSources"],
        }
        self.render(template_path("applist.html"), **data)


class NoPackagesHandler(BaseRequestHandler):
    async def get(self):
        update = await load_status("update")
        data = {
            "title": "No packages",
            "text": "We have metadata for these, but no apks at all.",
            "apps": update["data"].get("noPackages", []),
            "lastModified": update["lastModified"],
            "dataSources": update["dataSources"],
        }
        self.render(template_path("applist.html"), **data)


class MissingBuildsHandler(BaseRequestHandler):
    async def get(self):
        update = await load_status("update")
        data = {
            "title": "Missing builds",
            "text": "Those app versions still need to be build or failed and need to be fixed or disabled.",
            "fdroiddata": update["data"]["fdroiddata"]["commitId"],
            "apps": update["data"]["failedBuilds"],
            "lastModified": update["lastModified"],
            "dataSources": update["dataSources"],
        }
        self.render(template_path("appvclist.html"), **data)


class AntiFeaturesHandler(BaseRequestHandler):
    async def get(self):
        data = await load_status("update")

        # calculate number of apps with anti-features
        afset = set()
        for k, v in data["data"].get("antiFeatures", []).items():
            for afapp in v["apps"]:
                afset.add(afapp)
        data["total_app_count"] = len(afset)
        data["af_names"] = AF_NAMES

        self.render(template_path("anti-features.html"), **data)


class AntiFeatureHandler(BaseRequestHandler):
    async def get(self, anti_feature):
        d = await load_status("update")
        d["anti_feature"] = anti_feature
        d["anti_feature_name"] = AF_NAMES.get(anti_feature, anti_feature)
        self.render(template_path("anti-feature.html"), **d)


class DonationsHandler(BaseRequestHandler):
    async def get(self):
        date_data = []
        currency = self.get_query_argument("currency", default="dollar")
        date_data.append(datetime.datetime.today())
        date_data.append(self.get_query_argument("date_from", default="2017-01-01"))
        date_data.append(
            self.get_query_argument(
                "date_until", default=date_data[0].strftime("%Y-%m-%d")
            )
        )
        date_data.append(datetime.datetime.today().strftime("%Y-%m-%d"))
        date_data.append("2017-01-01")
        response, _ = await fdroidmonitor.fetcher.get(
            "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml",
            cache_duration=60 * 60,
        )
        root = ET.fromstring(response)

        namespace = {
            'gesmes': 'http://www.gesmes.org/xml/2002-08-01',
            'ecb': 'http://www.ecb.int/vocabulary/2002-08-01/eurofxref',
        }

        eur_usd_rate = float(
            root.find(".//ecb:Cube[@currency='USD']", namespace).attrib['rate']
        )

        response, _ = await fdroidmonitor.fetcher.get(
            "https://liberapay.com/F-Droid-Data/charts.json",
            cache_duration=60 * 60,
        )

        query = '''
        query (
        $account: [AccountReferenceInput!]
        $limit: Int!
        $kind: [TransactionKind]
        $dateFrom: DateTime
        $dateTo: DateTime
        ) {
        transactions(
            account: $account
            limit: $limit
            kind: $kind
            dateFrom: $dateFrom
            dateTo: $dateTo
        ) {
            nodes {
            createdAt
            amount {
                value
                currency
            }
            }
        }
        }
        '''

        variables = {
            "account": [
                {"slug": "f-droid"},
                {"slug": "f-droid-euro"},
            ],
            "limit": 10000,
            "dateFrom": "2010-01-10T00:00:00.000Z",
            "dateTo": date_data[3] + "T00:00:00.000Z",
            "kind": ["ADDED_FUNDS", "CONTRIBUTION"],
        }

        gqlQuery = {"query": query, "variables": variables}

        response2 = await fdroidmonitor.fetcher.post(
            "https://api.opencollective.com/graphql/v2",
            json_encode(gqlQuery),
            cache_name="open-collective-donations",
            headers={"Content-Type": "application/json"},
            cache_duration=60 * 60,
        )
        gplResp = json.loads(response2)

        monthly_donations = {}
        monthly_donationsOpen = {}
        high_value_monthly_donationsOpen = {}
        total_donationsOpen = 0
        total_donationsLibera = 0

        for transaction in gplResp['data']['transactions']['nodes']:
            date = datetime.datetime.strptime(
                transaction['createdAt'], '%Y-%m-%dT%H:%M:%S.%fZ'
            )

            if (
                date_data[1] is None
                or date >= datetime.datetime.strptime(date_data[1], '%Y-%m-%d')
            ) and (
                date_data[2] is None
                or date <= datetime.datetime.strptime(date_data[2], '%Y-%m-%d')
            ):
                month_year = date.strftime('%Y-%m')

                if currency == "dollar":
                    amount = (
                        transaction['amount']['value']
                        if transaction['amount']['currency'] == "USD"
                        else transaction['amount']['value'] * eur_usd_rate
                    )
                else:
                    amount = (
                        transaction['amount']['value']
                        if transaction['amount']['currency'] == "EUR"
                        else transaction['amount']['value'] * (1 / eur_usd_rate)
                    )
                if amount < 500:
                    total_donationsOpen += amount
                    if month_year in monthly_donationsOpen:
                        monthly_donationsOpen[month_year] += amount
                    else:
                        monthly_donationsOpen[month_year] = amount
                else:
                    if month_year in high_value_monthly_donationsOpen:
                        high_value_monthly_donationsOpen[month_year] += amount
                    else:
                        high_value_monthly_donationsOpen[month_year] = amount

        total_donationsOpen = round(total_donationsOpen, 2)

        data = {
            "date_from": date_data[1],
            "date_until": date_data[2],
            "totaldonations_open": total_donationsOpen,
        }

        data = json.loads(response)

        monthly_donations = defaultdict(float)

        for item in data:
            if datetime.datetime.strptime(
                item['date'], '%Y-%m-%d'
            ) >= datetime.datetime.strptime(
                date_data[1], '%Y-%m-%d'
            ) and datetime.datetime.strptime(
                item['date'], '%Y-%m-%d'
            ) <= datetime.datetime.strptime(
                date_data[2], '%Y-%m-%d'
            ):
                year, month, _ = item['date'].split('-')
                monthly_donations[f"{year}-{month}"] += float(
                    item['receipts']['amount']
                )
                total_donationsLibera += float(item['receipts']['amount'])

        total_donationsLibera = (
            round(total_donationsLibera, 2)
            if currency == "euro"
            else round(total_donationsLibera * eur_usd_rate, 2)
        )

        items = [
            {
                'month': k,
                'amount': (
                    round(v, 2) if currency == "euro" else round(v * eur_usd_rate, 2)
                ),
            }
            for k, v in monthly_donations.items()
        ]
        items2 = [
            {
                'month2': k,
                'amount2': [
                    round(v, 2),
                    round(high_value_monthly_donationsOpen.get(k, 0), 2),
                ],
            }
            for k, v in monthly_donationsOpen.items()
        ]

        self.render(
            template_path("donations.html"),
            items=items,
            items2=items2,
            total_open=total_donationsOpen,
            total_libera=total_donationsLibera,
            date_data=date_data,
            currency=currency,
            dataSources=[
                "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml",
                "https://api.opencollective.com/graphql/v2",
                "https://liberapay.com/F-Droid-Data/charts.json",
            ],
        )


class LogHandler(BaseRequestHandler):
    async def get(self, appid, vercode):
        log_url = "https://f-droid.org/repo/{}_{}.log.gz".format(appid, vercode)
        http_client = tornado.httpclient.AsyncHTTPClient()
        try:
            response = await http_client.fetch(log_url)
            if response.error:
                raise response.error
        except tornado.httpclient.HTTPClientError as e:
            raise MyException(
                "HTTP 404: Not Found", "no logs available for this build", 404
            ) from e
        else:
            data = {
                "appid": appid,
                "vercode": vercode,
                "log_lines": response.body.decode("utf-8").split("\n"),
                "dataSources": [log_url],
            }
            self.render(template_path("log.html"), **data)


def make_app(*, debug: bool):
    settings = {
        "debug": debug,
        "default_handler_class": Err404Handler,
        "template_loader": tornado.template.Loader(
            root_directory=os.path.join(
                os.path.abspath(os.path.dirname(__file__)), "templates"
            )
        ),
        "static_path": os.path.join(
            os.path.abspath(os.path.dirname(__file__)), "static"
        ),
        "ui_methods": fdroidmonitor.ui_methods,
    }
    return tornado.web.Application(
        (
            (r"/", IndexHandler),
            (r"/anti-feature/([a-zA-Z0-9_\.]+)", AntiFeatureHandler),
            (r"/anti-features", AntiFeaturesHandler),
            (r"/builds", BuildsHandler),
            (
                r"/builds/({})".format(
                    "|".join(
                        (
                            "running",
                            "build",
                            "pull-unsigned",
                            "push-repo",
                            "push-archive",
                            "signpkg",
                            "postpublish",
                            "update",
                            "makebs",
                            "signindex",
                            "postsignindex",
                            "deploy",
                        )
                    )
                ),
                StatusHandler,
            ),
            (r"/builds/archived", ArchivedHandler),
            (r"/builds/disabled", DisabledHandler),
            (r"/builds/log/([a-zA-Z0-9_\.]+)/([0-9]+)", LogHandler),
            (r"/builds/missingbuilds", MissingBuildsHandler),
            (r"/builds/needsupdate", NeedsUpdateHandler),
            (r"/builds/nopackages", NoPackagesHandler),
            (r"/builds/noupdatecheck", NoUpdateCheckHandler),
            (r"/builds/website", WebsiteHandler),
            (r"/donations", DonationsHandler),
            (r"/services", ServicesHandler),
            (r"/services/tls-certs", TlsCertsHandler),
            (r"/website", tornado.web.RedirectHandler, {"url": "/builds/website"}),
        ),
        **settings,
    )


async def shutdown():
    # http_server.stop()
    # for client in ws_clients.values():
    #     client['handler'].close()
    # await tornado.gen.sleep(1)
    tornado.ioloop.IOLoop.current().stop()


def exit_handler(sig, frame):
    print("shutting down ...")
    fdroidmonitor.services.RUNNING = False
    fdroidmonitor.tls_certs.RUNNING = False
    tornado.ioloop.IOLoop.instance().add_callback_from_signal(shutdown)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--debug",
        "-d",
        action="store_true",
        default=False,
        help="If this flag is present, the app will start in debug mode. This is only useful for development, never activate this on your servers.",
    )
    parser.add_argument(
        "--port",
        "-p",
        type=int,
        help="set the network port on which this application server is supposed to listen on",
    )
    parser.add_argument(
        "--address",
        "-a",
        type=str,
        help="set the network address on which this application server is supposed to listen on",
    )
    parser.add_argument(
        "--config", "-c", type=str, help="load configuration from this file"
    )
    args = parser.parse_args()

    cfg = fdroidmonitor.cfg.load(args=args)

    if cfg["debug"]:
        print("starting in debug mode ...")
    app = make_app(debug=cfg["debug"])
    app.listen(cfg["port"], cfg["address"])

    signal.signal(signal.SIGTERM, exit_handler)
    signal.signal(signal.SIGINT, exit_handler)

    print("listening for requests on: http://{}:{}".format(cfg["address"], cfg["port"]))
    tornado.ioloop.IOLoop.current().spawn_callback(fdroidmonitor.services.services_loop)
    tornado.ioloop.IOLoop.current().spawn_callback(
        fdroidmonitor.tls_certs.services_loop
    )
    tornado.ioloop.IOLoop.current().start()
