# SPDX-FileCopyrightText: 2023 Michael Pöhn <michael.poehn@fsfe.org>
# SPDX-License-Identifier: AGPL-3.0-or-later

import time
import tornado.httpclient

GET_CACHE = {}
POST_CACHE = {}


async def get(url, cache_duration=30):
    if time.time() > GET_CACHE.get(url, {}).get("timestamp", 0) + cache_duration:
        # update timestamp instantly to make sure we don't send multiple
        # network requests at the same time.
        GET_CACHE.get(url, {})["timestamp"] = time.time()
        # might not be the most efficient way to get a http client instance but
        # seems to work good enough for our purposes
        http_client = tornado.httpclient.AsyncHTTPClient()

        response = await http_client.fetch(url)
        GET_CACHE[url] = {
            "timestamp": time.time(),
            'body': response.body,
            'last-modified': response.headers["last-modified"],
        }

        # if there was an error, just raise it, BaseRequestHandler will render
        # it accordingly
        if response.error:
            raise response.error

    return GET_CACHE[url]['body'], GET_CACHE[url]["last-modified"]


async def post(url, body, headers={}, cache_name="", cache_duration=30):
    if not cache_name:
        raise Exception("cache name programming errro")

    if (
        time.time()
        > POST_CACHE.get(cache_name, {}).get("timestamp", 0) + cache_duration
    ):
        # update timestamp instantly to make sure we don't send multiple
        # network requests at the same time.
        POST_CACHE.get(cache_name, {})["timestamp"] = time.time()
        # might not be the most efficient way to get a http client instance but
        # seems to work good enough for our purposes
        http_client = tornado.httpclient.AsyncHTTPClient()

        response = await http_client.fetch(
            url,
            method='POST',
            body=body,
            headers=headers,
        )
        POST_CACHE[cache_name] = {
            "timestamp": time.time(),
            'body': response.body,
        }

        # if there was an error, just raise it, BaseRequestHandler will render
        # it accordingly
        if response.error:
            raise response.error

    return POST_CACHE[cache_name]['body']
